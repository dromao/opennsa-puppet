# OpenNSA client puppet manifest
# Tested on CentOS 6.5 and CentOS 7

# Where the rpm is hosted
$webserver="145.100.132.185"

# Change hostname to match your system
node localhost {

	# Create OpenNSA user
	user { 'opennsa':
	    ensure => "present",
	    home => "/home/opennsa",
	    shell => "/bin/bash",
	    managehome => true,
	}

	# Install OpenNSA dependencies:

	package { 'gcc':
	    ensure => 'installed',
	}

	case $operatingsystemrelease {
	    /^6.*/: {

			yumrepo { 'PUIAS':
			    mirrorlist => "http://puias.math.ias.edu/data/puias/computational/$operatingsystemrelease/$architecture/mirrorlist",
			    descr => "PUIAS repository",
			    enabled => 1,
			    gpgcheck => 0,
			    ensure   => 'present',
			}

			package { 'python27-devel':
			    ensure => 'installed',
			    require => YUMREPO ['PUIAS'],
			}

			package { 'python27-setuptools':
			    ensure => 'installed',
			    require => PACKAGE ['python27-devel'],
			}

			exec { 'Install-pip':
			    command => "/usr/bin/easy_install-2.7 pip && /usr/sbin/alternatives --install /usr/bin/pip-python pip-python /usr/bin/pip2.7 1",
			    require => PACKAGE ['python27-setuptools'],
			}
	    }
	    /^7.*/: {

	        package { 'python-devel':
			    ensure => 'installed',
			}

	    	exec { 'Install-pip':
			    command => "/usr/bin/easy_install-2.7 pip && /usr/sbin/alternatives --install /usr/bin/pip-python pip-python /usr/bin/pip2.7 1",
			    require => PACKAGE ['python-devel'],
			}
	    }
	}

	package { 'libffi-devel':
	    ensure => 'installed',
	}

	package { 'openssl-devel':
	    ensure => 'installed',
	}
	
	package { 'python-dateutil==1.5':
	    ensure  => installed,
	    provider => pip,
	    require => EXEC ['Install-pip'],
	}

	package { 'pyopenssl':
	    ensure  => installed,
	    provider => pip,
	    require => [ EXEC ['Install-pip'], PACKAGE ['gcc'], PACKAGE ['libffi-devel'], PACKAGE ['openssl-devel'] ],
	}

	package { 'twisted':
	    ensure  => installed,
	    provider => pip,
	    require => [ EXEC ['Install-pip'], PACKAGE ['gcc'] ],
	}

	package { 'service_identity':
	    ensure  => installed,
	    provider => pip,
	    require => EXEC ['Install-pip'],
	}

	# Install OpenNSA rpm
	package { 'opennsa':
	    ensure => 'installed',
	    source => "http://${webserver}/opennsa.rpm",
	    provider => 'rpm',
	    require => EXEC ['Install-pip'],
	}

}