# OpenNSA server puppet manifest
# Tested on CentOS 6.5 and CentOS 7

# Dependencies:
# puppet module install puppetlabs-postgresql

# Where the rpm and database schema are hosted
$webserver="145.100.132.185"

# Change hostname to match your system
node localhost {

	# Create OpenNSA user
	user { 'opennsa': 
	    ensure => "present", 
	    home => "/home/opennsa", 
	    shell => "/bin/bash", 
	    managehome => true, 
	}

	# Install OpenNSA dependencies:

	package { 'ntp': 
	    ensure  => installed,
	} 

	package { 'gcc':
	    ensure => 'installed',
	}

	case $operatingsystemrelease {
	    /^6.*/: {

			yumrepo { 'PUIAS':
			    mirrorlist => "http://puias.math.ias.edu/data/puias/computational/$operatingsystemrelease/$architecture/mirrorlist",
			    descr => "PUIAS repository",
			    enabled => 1,
			    gpgcheck => 0,
			    ensure   => 'present',
			}

			package { 'python27-devel':
			    ensure => 'installed',
			    require => YUMREPO ['PUIAS'],
			}

			package { 'python27-setuptools':
			    ensure => 'installed',
			    require => PACKAGE ['python27-devel'],
			}

			exec { 'Install-pip':
			    command => "/usr/bin/easy_install-2.7 pip && /usr/sbin/alternatives --install /usr/bin/pip-python pip-python /usr/bin/pip2.7 1",
			    require => PACKAGE ['python27-setuptools'],
			}
	    }
	    /^7.*/: {

	        package { 'python-devel':
			    ensure => 'installed',
			}

	    	exec { 'Install-pip':
			    command => "/usr/bin/easy_install-2.7 pip && /usr/sbin/alternatives --install /usr/bin/pip-python pip-python /usr/bin/pip2.7 1",
			    require => PACKAGE ['python-devel'],
			}
	    }
	}

	package { 'redhat-lsb':
	    ensure => 'installed',
	}

	package { 'libffi-devel':
	    ensure => 'installed',
	}

	package { 'openssl-devel':
	    ensure => 'installed',
	}
	
	package { 'python-dateutil==1.5':
	    ensure  => installed,
	    provider => pip,
	    require => EXEC ['Install-pip'],
	}

	package { 'pyopenssl':
	    ensure  => installed,
	    provider => pip,
	    require => [ EXEC ['Install-pip'], PACKAGE ['gcc'], PACKAGE ['libffi-devel'], PACKAGE ['openssl-devel'] ],
	}

	package { 'twisted':
	    ensure  => installed,
	    provider => pip,
	    require => [ EXEC ['Install-pip'], PACKAGE ['gcc'] ],
	}

	package { 'twistar':
	    ensure  => installed,
	    provider => pip,
	    require => EXEC ['Install-pip'],
	}

	package { 'psycopg2':
	    ensure  => installed,
	    provider => pip,
	    require => [ EXEC ['Install-pip'], CLASS ['postgresql::lib::devel'], PACKAGE ['gcc'] ],
	}

	package { 'pycrypto':
	    ensure  => installed,
	    provider => pip,
	    require => [ EXEC ['Install-pip'], PACKAGE ['gcc'] ],
	}

	package { 'service_identity':
	    ensure  => installed,
	    provider => pip,
	    require => EXEC ['Install-pip'],
	}

	# Install OpenNSA rpm
	package { 'opennsa':
	    ensure => 'installed',
	    source => "http://${webserver}/opennsa.rpm",
	    provider => 'rpm',
	    require => EXEC ['Install-pip'],
	}

	# Install and configure PostgreSQL

	class {'postgresql::lib::python':
	    package_name => 'postgresql-plpython',
	}

	class { 'postgresql::server': 
	    needs_initdb => 'true',
	}

	class { 'postgresql::lib::devel': }

	postgresql::server::role { 'opennsa':
	    password_hash => postgresql_password('opennsa', ''),
	}

	postgresql::server::db { 'opennsa':
	    user     => 'opennsa',
	    password => '',
	    owner => 'opennsa',
	    require => POSTGRESQL::SERVER::ROLE [ 'opennsa' ],
	}

	exec { 'db-schema':
	    command => "/usr/bin/curl -o /tmp/opennsa-schema.sql http://${webserver}/opennsa-schema.sql && psql opennsa -f /tmp/opennsa-schema.sql",
	    user => 'opennsa',
	    require => [ POSTGRESQL::SERVER::DB ['opennsa'], USER ['opennsa'] ],
	}
}